from sys import argv
from calculator.simple import SimpleCalculator
import sys


def calc(text):
    try:
        c = SimpleCalculator()
        c.run(text)
        # print(c.log)
        return c.log[-1].split(" ")[-1]
    except Exception as e:
        print(e)
        return 0.0


sys.stdout.flush()

if __name__ == '__main__':
    print(calc(argv[1]))
