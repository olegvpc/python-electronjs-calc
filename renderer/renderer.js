let input = document.querySelector('#input')
let result = document.querySelector('#result')
let btn = document.querySelector('#btn')


function handleClick() {
  // console.log(input.value)
  ipcRenderer.send('python:request', input.value)
}

ipcRenderer.on('python:result', (data) => {
  result.textContent = data
  input.value = ""
})


btn.addEventListener('click', () => {
  handleClick();
});

