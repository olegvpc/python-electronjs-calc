from sys import argv


def calc(text):
    # print("from python", text, type(text))
    try:
        text_list = text.split(' ')
        op = text_list[1]
        if(op == '+'):
            return int(text_list[0]) + int(text_list[2])
        elif(op == "-"):
            return int(text_list[0]) - int(text_list[2])
        elif(op == "*"):
            return int(text_list[0]) * int(text_list[2])
        elif(op == "/"):
            return int(text_list[0]) / int(text_list[2])
    except Exception as e:
        print(e)
        return 0.0


if __name__ == '__main__':
    print(calc(argv[1]))