const { app, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const os = require("os");
const express = require("express");

process.env.NODE_ENV = 'production1'


const isDev = process.env.NODE_ENV !== 'production';
const isMac = process.platform === 'darwin';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) {
//   app.quit();
// }

let mainWindow
let streamWindow

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: isDev ? 1200 : 600,
    // width: 600,
    height: 600,
    title: "CALCULATOR",
    // icon: `${__dirname}/icons/mac/icon.icns`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, '/preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

  // Open the DevTools.
  if(isDev) {
    mainWindow.webContents.openDevTools();
  }
};

ipcMain.on('python:request', (e, input) => {
  sendToPython(input);
});
function sendToPython(input) {
  console.log("function sendToPython", input, __dirname)
  let { PythonShell } = require('python-shell');
  // IMPORTANT - PATH TO PYTHON AND PATH TO SCRIPT
  let options = {
    mode: 'text',
    pythonPath: '/Users/olegvpc/Web/FW-Electron/python-electronjs-calc/venv/bin/python3',
    pythonOptions: ['-u'], // get print results in real-time
    scriptPath: '/Users/olegvpc/Web/FW-Electron/python-electronjs-calc/python/',
    args: [input]
  };
  PythonShell.run('calc.py', options)
    .then((messages)=> {
      console.log("from PythonShell", messages)
      mainWindow.webContents.send("python:result", messages)
    })
    .catch((err) => console.log(err))
}

app.on('ready', () => {
  createWindow()

  mainWindow.on('closed', () => (mainWindow = null));
});

app.on('window-all-closed', () => {
  if (!isMac) {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});