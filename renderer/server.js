const express = require('express');

const app = express()

const {
  PORT = 4000,
  BASE_PATH = 'http://localhost:4000',
} = process.env;

app.get('/', (req, res) => {
  res.send("EVERYTHING is OK")
});


app.listen(PORT, () => {
  console.log('Ссылка на сервер');
  console.log(BASE_PATH);
});