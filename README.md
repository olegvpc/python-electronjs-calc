
```shell
python3 -m venv venv

source venv/bin/activate

python3 -m pip install --upgrade pip
```


### Установить зависимости из файла requirements.txt:
```shell
pip3 install -r requirements.txt
```

### just for INFO- (Record requirements.txt)
```shell
pip3 freeze > requirements.txt
```
### install npm auto
```shell
npm inslall
```
### or manual
```shell
npm install --save-dev electron
npm install --save-dev electronmon
npm install --save-dev electron-packager
npm i python-shell

```

* package.json
```json
  "devDependencies": {
    "electron": "^23.0.0",
    "electron-packager": "^17.1.1"
  },
  "dependencies": {
    "electronmon": "^2.0.2",
    "os": "^0.1.2",
    "path": "^0.12.7",
    "python-shell": "^5.0.0"
  }
### for develop
```
### dev Electron
```shell
npm run dev
```
### for packeging App
```
npx electron-packager .
```



