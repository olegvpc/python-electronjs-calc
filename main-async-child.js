const { app, BrowserWindow, ipcMain} = require('electron');
const path = require('path');
const os = require("os");
// const {PythonShell} = require("python-shell");

process.env.NODE_ENV = 'production1'
// process.env.PATH = path.join(__dirname, '/python')
console.log(process.env.PATH)

const isDev = process.env.NODE_ENV !== 'production';
const isMac = process.platform === 'darwin';

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
// if (require('electron-squirrel-startup')) {
//   app.quit();
// }

let mainWindow
let streamWindow

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: isDev ? 1200 : 600,
    // width: 600,
    height: 600,
    title: "CALCULATOR",
    // icon: `${__dirname}/icons/mac/icon.icns`,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      preload: path.join(__dirname, '/preload.js'),
    },
  });

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));

  // Open the DevTools.
  if(isDev) {
    mainWindow.webContents.openDevTools();
  }
};

ipcMain.on('python:request', (e, input) => {
  sendToPython(input);
});
function sendToPython(input) {
  console.log(input)
  runServer(input)

}

function runServer (input) {
  const express = require("express");
  const { spawn } = require('child_process')

  const app = express()

  const {
    PORT = 3000,
    BASE_PATH = 'http://localhost:3000',
  } = process.env;

  const executePython = async (script, args) => {
    // const arguments = args.map(arg => arg.toString())
    // const python = spawn("python", [script, ...arguments])
    const py = spawn("python3", [script, args], {
      env: {
        NODE_ENV: 'production',
        PATH: process.env.PATH
      }
    })

    const result = new Promise((resolve, reject) => {
      let output

      py.stdout.on('data', (data) => {
        output = JSON.parse(data)
        // console.log(output) // 36
      });

      py.stderr.on('data', (data) => {
        console.error(`[python] Error occured in ${data}`)
        reject(`Error occured in ${script}`)
      });

      py.on("exit", (code) => {
        console.log(`Child process exit with code: ${code}`)
        resolve(output)
      });
    });
  return result
  }

  const result = async () => {
    await executePython('/Users/olegvpc/Web/FW-Electron/python-electronjs-calc/python/calc.py', [input])
  }
  mainWindow.webContents.send("python:result", result)

  // app.get('/', async (req, res) => {
  //   // res.send("Test is OK")
  //   try {
  //     const result = await executePython('/Users/olegvpc/Web/FW-Electron/python-electronjs-calc/python/calc.py', [input])
  //     // console.log(result)
  //     res.json({result: result})
  //     mainWindow.webContents.send("python:result", result)
  //   } catch (error) {
  //     res.status(500).json({error: error})
  //   }
  // });


  app.listen(PORT, () => {
    console.log('Ссылка на сервер');
    console.log(BASE_PATH);
  });
}


app.on('ready', () => {
  createWindow()

  mainWindow.on('closed', () => (mainWindow = null));
});

app.on('window-all-closed', () => {
  if (!isMac) {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
